from flask_migrate import Migrate

from app import blueprint
from app.main.commands import commands_bp
from app.main import create_app, db
from app.main.model import data, user, sensor, blacklisted

app = create_app()
app.register_blueprint(blueprint)
app.register_blueprint(commands_bp)

migrate = Migrate(app, db)

if __name__ == '__main__':
     app.run()